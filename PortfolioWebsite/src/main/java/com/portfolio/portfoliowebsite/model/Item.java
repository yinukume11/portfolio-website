package com.portfolio.portfoliowebsite.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

@Component
public class Item {
    @Autowired
    ResourceLoader resourceLoader;

    private String imageUrl;
    private String alt;
    private String caption;

    public Item(String imageUrl, String alt, String caption) {
        this.imageUrl = imageUrl;
        this.alt = alt;
        this.caption = caption;
    }

    public Item() {
        this("", "", "");
    }

    public ArrayList<Item> giveItems(String url, String type) throws IOException {
        ArrayList<Item> output = new ArrayList<>();

        Resource fileResource = resourceLoader.getResource(url);
        InputStream inputStream = fileResource.getInputStream();
        Scanner scanner = new Scanner(inputStream);

        int counter = 1;
        Item currentItem = new Item();

        while (scanner.hasNext()) {
            String line = scanner.nextLine();

            if (counter % 3 == 0) {
                currentItem.setCaption(line);
                output.add(currentItem);

                currentItem = new Item();
                counter = 0;
            }
            else if (counter % 2 == 0) {
                currentItem.setAlt(line);
            }
            else {
                String clsspth = "css/images/portfolioimages/";
                currentItem.setImageUrl(clsspth + type + line);
            }

            counter++;
        }

        return output;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
