package com.portfolio.portfoliowebsite.model;

import org.springframework.stereotype.Component;

import java.util.InputMismatchException;

@Component
public class LanguageState {
    private String language = "EN";

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        if (language.equals("NL") || language.equals("EN")) {
            this.language = language;
        }
        else {
            throw new InputMismatchException("Language can either be NL or EN");
        }
    }
}
