package com.portfolio.portfoliowebsite.controllers;

import com.portfolio.portfoliowebsite.model.LanguageState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.web.bind.annotation.*;

@RestController
@DependsOn("languageState")
public class LanguageController {
    private LanguageState languageState;

    @Autowired
    public LanguageController(LanguageState languageState) {
        this.languageState = languageState;
    }

    @GetMapping(value = "/getlanguage")
    public String currentLanguage() {
        System.out.println(languageState.getLanguage());
        return languageState.getLanguage();
    }

    @GetMapping(value = "/postlanguage/{language}")
    public String changeLanguage(@PathVariable("language") String language) {
        System.out.println(languageState.getLanguage());
        languageState.setLanguage(language);
        return languageState.getLanguage();
    }
}
