package com.portfolio.portfoliowebsite.controllers;

import com.portfolio.portfoliowebsite.model.Item;
import com.portfolio.portfoliowebsite.model.LanguageState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;

@Controller
@DependsOn("languageState")
public class ViewController {
    @Autowired
    private Item baseItem = new Item();

    private LanguageState languageState;

    @Autowired
    public ViewController(LanguageState languageState) {
        this.languageState = languageState;
    }

    @GetMapping({"/", "/home"})
    public String getHome(Model model) {
        model.addAttribute("language", languageState.getLanguage());
        return "home";
    }

    @GetMapping("/game-art")
    public String getGameArt(Model model, RedirectAttributes redirectAttrs) {
        try {
            model.addAttribute("language", languageState.getLanguage());
            ArrayList<Item> galleryItems = baseItem.giveItems("classpath:static/galleryitems/gameart.txt", "gameart/");
            model.addAttribute("galleryItems", galleryItems);
            return "game-art";
        }
        catch (Exception e) {
            redirectAttrs.addFlashAttribute("errorText", e.getMessage());
            return "redirect:/error";
        }
    }

    @GetMapping("/digital-art")
    public String getDigitalArt(Model model, RedirectAttributes redirectAttrs) {
        try {
            model.addAttribute("language", languageState.getLanguage());
            ArrayList<Item> galleryItems = baseItem.giveItems("classpath:static/galleryitems/digitalart.txt", "digitalart/");
            model.addAttribute("galleryItems", galleryItems);
            return "digital-art";
        }
        catch (Exception e) {
            redirectAttrs.addFlashAttribute("errorText", e.getMessage());
            return "redirect:/error";
        }
    }

    @GetMapping("/webcomics")
    public String getWebcomics(Model model, RedirectAttributes redirectAttrs) {
        try {
            model.addAttribute("language", languageState.getLanguage());
            ArrayList<Item> galleryItems = baseItem.giveItems("classpath:static/galleryitems/webcomics.txt", "webtoons/");
            model.addAttribute("galleryItems", galleryItems);
            return "webcomics";
        }
        catch (Exception e) {
            redirectAttrs.addFlashAttribute("errorText", e.getMessage());
            return "redirect:/error";
        }
    }

    @GetMapping("/contact")
    public String getContactPage(Model model) {
        model.addAttribute("language", languageState.getLanguage());
        return "contact";
    }

    @GetMapping("/error")
    public String getErrorPage(Model model) {
        model.addAttribute("errorText", "");
        return "error";
    }

    @PostMapping("/error")
    public String getErrorPageWithText(@ModelAttribute String errorText, Model model) {
        model.addAttribute("errorText", errorText);
        return "error";
    }
}
