let buttonEn;
let buttonNl;

window.onload = function () {
    buttonEn = document.getElementById("englishButton");
    buttonNl = document.getElementById("dutchButton");

    fetchLanguageData();
}

function toggleLanguage(language) {
    setActiveDivs(language)
    console.log("Language before post: "+ language);
    postLanguageData(language);
}

function setActiveDivs(language) {
    if (language === 'NL') {
        buttonNl.classList.add("active");
        buttonEn.classList.remove("active");
    }
    else {
        buttonEn.classList.add("active");
        buttonNl.classList.remove("active");
    }
}

function fetchLanguageData() {
    fetch("getlanguage")
        .then(response => response.text())
        .then(txt => {
                console.log("Language after fetch = " + txt)
                setActiveDivs(txt);
            }
        )
        .catch((error) => {
            console.log("Oops", error);
        })
}

function postLanguageData(language) {
    console.log("Language in postlanguagedata = " + language)

    let url = "postlanguage/" + language;
    fetch(url)
        .then(response =>  response.text())
        .then(txt => {
                console.log("Language after post = " + txt);
                location.reload();
            }
        )
        .catch((error) => {
            console.log("Oops", error);
        })
}