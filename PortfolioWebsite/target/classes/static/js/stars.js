// Twinkling stars code by https://jsfiddle.net/psullivan6/ma6e78m0/

window.onload = function() {
    generateStars();
};

onresize = (event) => {
    let galaxy = document.getElementsByClassName('galaxy')[0];
    while (galaxy.firstChild) {
        galaxy.removeChild(galaxy.firstChild);
    }

    generateStars();
};

// Random Stars
let generateStars = function(){

    let $galaxy = $(".galaxy");
    let iterator = 0;

    while (iterator <= 100){
        let xposition = Math.random();
        let yposition = Math.random();
        let star_type = Math.floor((Math.random() * 3) + 1);
        let position = {
             "x" : (window.innerWidth - 50) * xposition,
             "y" : (window.innerHeight - 20) * yposition,
        };

        $('<div class="star star-type' + star_type + '"></div>').appendTo($galaxy).css({
            "top" : position.y,
            "left" : position.x
        });

        iterator++;
    }

};

// generateStars();