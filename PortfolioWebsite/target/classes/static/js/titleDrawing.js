let ctx;

let cw;
let ch;

let angleOuter;
let angleInner;

let arcStart;

let radiusOuter;
let radiusInner;

window.onload = function() {
    init();
};

function init() {
    ctx = document.getElementById("canvas").getContext("2d");
    cw = document.getElementById("canvas").width;
    ch = document.getElementById("canvas").height;

    arcStart = 10 * Math.PI / 6;
    angleOuter = arcStart + Math.PI / 15;
    angleInner = arcStart;

    radiusOuter = cw * (105 / 300);
    radiusInner = cw * (77 / 300);

    window.requestAnimationFrame(animate);
}

function draw() {
    ctx.globalCompositeOperation = "destination-over";

    ctx.lineWidth = 4;
    ctx.strokeStyle = "rgba(0, 153, 255, 1)";

    let endOuter = arcStart + Math.PI / 18;
    let endInner = arcStart;

    ctx.beginPath();
    ctx.arc(cw / 2, ch / 2, radiusOuter, angleOuter, endOuter, false);
    ctx.stroke();
    ctx.closePath();

    ctx.beginPath();
    ctx.arc(cw / 12 * 7, ch / 96 * 43, radiusInner, angleInner, endInner, false);
    ctx.stroke();
    ctx.closePath();
}

function animate() {
    ctx.clearRect(0, 0, cw, ch);
    angleOuter -= Math.PI / 120;
    angleInner -= Math.PI / 150;

    if (angleOuter < 0 - Math.PI / 18) {
        angleOuter = -Math.PI / 18;
    }

    if (angleInner < 0) {
        angleInner = 0;
    }

    draw();
    drawLetters();
    window.requestAnimationFrame(animate)
}

function drawLetters() {
    ctx.beginPath();
    // Y:
    ctx.moveTo(cw / 16 * 9, ch / 16 * 5);
    ctx.quadraticCurveTo(cw / 5 * 3, ch / 8 * 3, cw / 16 * 7, ch / 8 * 5);
    ctx.moveTo(cw / 16 * 7, ch / 16 * 5);
    ctx.quadraticCurveTo(cw / 16 * 7 , ch / 4, cw / 32 * 17, ch / 32 * 15);

    // U:
    ctx.moveTo(cw / 64 * 45, ch / 32 * 14);
    ctx.quadraticCurveTo(cw / 64 * 41, ch / 32 * 24, cw / 64 * 47, ch / 32 * 18);
    ctx.moveTo(cw / 64 * 37, ch / 32 * 14);
    ctx.quadraticCurveTo(cw / 32 * 16, ch / 32 * 23, cw / 64 * 43, ch / 64 * 39);
    ctx.stroke();
}